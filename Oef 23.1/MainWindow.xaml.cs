﻿using System;
using System.Windows;

namespace Oef_23._1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

        }

        private void btnGooien_Click(object sender, RoutedEventArgs e)
        {
            Random random = new Random();
            lblBlauw.Content += Convert.ToString(random.Next(1, 9));
            lblRood.Content += Convert.ToString(random.Next(1, 9));
        }
    }
}
